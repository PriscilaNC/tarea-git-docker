package  org.example;

public class Main {
    public static void main(String[] args) {
        TaskList taskList = new TaskList();

        Task taskAlfa = new Task("Alfa", "Tarea uno", true);
        Task taskBeta = new Task("Beta", "Tarea dos", false);

        taskList.addTask(taskAlfa);
        taskList.addTask(taskBeta);

        taskList.removeTask(taskBeta);
    }
}