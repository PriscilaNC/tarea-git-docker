FROM openjdk
WORKDIR /app
COPY . .
CMD ["java", "/app/src/main/java/Main.java"]